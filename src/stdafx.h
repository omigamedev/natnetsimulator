#pragma once

#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#define GLM_FORCE_RADIANS
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL

#include "targetver.h"

#include <Windows.h>
#include <stdio.h>
#include <tchar.h>

#include <map>
#include <deque>
#include <memory>
#include <chrono>
#include <thread>
#include <mutex>
#include <vector>
#include <algorithm>
#include <functional>
#include <condition_variable>

#include <GL/glew.h>
#include <GL/wglew.h>
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <stb.h>
#include <stb_image.h>

#include <NatNetTypes.h>
#include <NatNetServer.h>
