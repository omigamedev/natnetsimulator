#pragma once

#include "Shader.h"
#include "Primitive.h"
#include "ShaderManager.h"
#include "RenderTexture.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb.h>
#include <stb_image.h>

bool init();
void render();
void window_close(GLFWwindow* window);
void window_resize(GLFWwindow* window, int w, int h);
void window_key(GLFWwindow* window, int key, int scancode, int action, int mods);
void window_cursor(GLFWwindow* window, double x, double y);
void window_button(GLFWwindow* window, int button, int action, int mods);
int main(int argc, char* argv[]);

GLFWwindow* window = nullptr;
HINSTANCE hInst;

int g_width = 800;
int g_height = 800;
bool active = false;

struct Cursor
{
    static glm::vec2 pos;
    glm::vec2 dragStart;
    glm::vec2 dragDelta;
    bool dragging;
    bool down;
};
glm::vec2 Cursor::pos;

Cursor mouseLeft;
Cursor mouseRight;
ShaderManager sm;
RenderTexture rtt;
Plane plane;
Sphere sphere;
Cube cube;
