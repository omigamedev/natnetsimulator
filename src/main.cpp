#include "stdafx.h"
#include "main.h"

glm::mat4 proj;
glm::mat4 camera;
std::unique_ptr<NatNetServer> server;
sDataDescriptions descriptions;
long g_lCurrentFrame = 0;

void ServerLog(int type, char* msg)
{
    printf("Log: %s\n", msg);
}

int ServerRequestHandler(sPacket* pPacketIn, sPacket* pPacketOut, void* pUserData)
{
    int iHandled = 1;	// handled

    switch (pPacketIn->iMessage)
    {
    case NAT_PING:
        printf("received ping from Client.\n");

        // build server info packet
        strcpy(pPacketOut->Data.Sender.szName, "Simulator Server");
        pPacketOut->Data.Sender.Version[0] = 0;
        pPacketOut->Data.Sender.Version[1] = 1;
        pPacketOut->iMessage = NAT_PINGRESPONSE;
        pPacketOut->nDataBytes = sizeof(sSender);
        iHandled = 1;
        break;

    case NAT_REQUEST_MODELDEF:
        printf("Received request for data descriptions.\n");
        server->PacketizeDataDescriptions(&descriptions, pPacketOut);
        break;

    case NAT_REQUEST_FRAMEOFDATA:
    {
        // note: Client does not typically poll for data, but we accomodate it here anyway
        // note: need to return response on same thread as caller
        printf("Received request for frame of data.\n");
        sFrameOfMocapData frame;
        //BuildFrame(g_lCurrentFrame, &descriptions, &frame);
        server->PacketizeFrameOfMocapData(&frame, pPacketOut);
        //FreeFrame(&frame);
    }
    break;

    case NAT_REQUEST:
        printf("Received request from Client: %s\n", pPacketIn->Data.szData);
        pPacketOut->iMessage = NAT_UNRECOGNIZED_REQUEST;
        if (_stricmp(pPacketIn->Data.szData, "TestRequest") == 0)
        {
            pPacketOut->iMessage = NAT_RESPONSE;
            strcpy(pPacketOut->Data.szData, "TestResponse");
            pPacketOut->nDataBytes = ((int)strlen(pPacketOut->Data.szData)) + 1;
        }
        break;

    default:
        pPacketOut->iMessage = NAT_UNRECOGNIZED_REQUEST;
        pPacketOut->nDataBytes = 0;
        iHandled = 0;
        break;
    }

    return iHandled; // 0 = not handled, 1 = handled;
}


// Build a DataSet description (MarkerSets, RigiBodies, Skeletons).
void BuildDescription(sDataDescriptions* pDescription)
{
    pDescription->nDataDescriptions = 0;

    // Rigid Body Description
    for (int i = 0; i < 1; i++)
    {
        sRigidBodyDescription* pRigidBodyDescription = new sRigidBodyDescription();
        sprintf(pRigidBodyDescription->szName, "RigidBody %d", i);
        pRigidBodyDescription->ID = i;
        pRigidBodyDescription->offsetx = 1.0f;
        pRigidBodyDescription->offsety = 2.0f;
        pRigidBodyDescription->offsetz = 3.0f;
        pRigidBodyDescription->parentID = 2;
        pDescription->arrDataDescriptions[i].type = Descriptor_RigidBody;
        pDescription->arrDataDescriptions[i].Data.RigidBodyDescription = pRigidBodyDescription;
        pDescription->nDataDescriptions++;
    }
}

// Send DataSet description to Client
void SendDescription(sDataDescriptions* pDescription)
{
    sPacket packet;
    server->PacketizeDataDescriptions(pDescription, &packet);
    server->SendPacket(&packet);
}


// Build frame of MocapData
void BuildFrame(long FrameNumber, sDataDescriptions* pModels, sFrameOfMocapData* pOutFrame)
{
    static float angle = 0;
    angle += 0.03f;

    if (!pModels)
    {
        printf("No models defined - nothing to send.\n");
        return;
    }

    ZeroMemory(pOutFrame, sizeof(sFrameOfMocapData));
    pOutFrame->iFrame = FrameNumber;
    pOutFrame->fLatency = (float)GetTickCount();
    pOutFrame->nOtherMarkers = 0;
    pOutFrame->nMarkerSets = 0;
    pOutFrame->nRigidBodies = 0;
    pOutFrame->nLabeledMarkers = 0;

    for (int i = 0; i < pModels->nDataDescriptions; i++)
    {
        // RigidBody data
        if (pModels->arrDataDescriptions[i].type == Descriptor_RigidBody)
        {
            sRigidBodyDescription* pMS = pModels->arrDataDescriptions[i].Data.RigidBodyDescription;
            int index = pOutFrame->nRigidBodies;
            sRigidBodyData* pRB = &pOutFrame->RigidBodies[index];

            pRB->ID = pMS->ID;
            pRB->x = Cursor::pos.x - g_width / 2;
            pRB->y = Cursor::pos.y - g_height / 2;
            pRB->z = 0;// sin(angle) * 100 + 100;

            glm::quat q(glm::eulerAngleZ(angle));
            pRB->qx = q.x;
            pRB->qy = q.y;
            pRB->qz = q.z;
            pRB->qw = q.w;

            pRB->nMarkers = 3;
            pRB->Markers = new MarkerData[pRB->nMarkers];
            pRB->MarkerIDs = new int[pRB->nMarkers];
            pRB->MarkerSizes = new float[pRB->nMarkers];
            pRB->MeanError = 0.0f;
            for (int iMarker = 0; iMarker < pRB->nMarkers; iMarker++)
            {
                pRB->Markers[iMarker][0] = iMarker + 0.1f;		// x
                pRB->Markers[iMarker][1] = iMarker + 0.2f;		// y
                pRB->Markers[iMarker][2] = iMarker + 0.3f;		// z
                pRB->MarkerIDs[iMarker] = iMarker + 200;
                pRB->MarkerSizes[iMarker] = 77.0f;
            }
            pOutFrame->nRigidBodies++;
        }
    }
}

// Packetize and send a single frame of mocap data to the Client
void SendFrame(sFrameOfMocapData* pFrame)
{
    sPacket packet;
    server->PacketizeFrameOfMocapData(pFrame, &packet);
    server->SendPacket(&packet);
}

// Release any memory we allocated for this sample
void FreeFrame(sFrameOfMocapData* pFrame)
{
    for (int i = 0; i < pFrame->nMarkerSets; i++)
    {
        delete[] pFrame->MocapData[i].Markers;
    }

    for (int i = 0; i < pFrame->nOtherMarkers; i++)
    {
        delete[] pFrame->OtherMarkers;
    }

    for (int i = 0; i < pFrame->nRigidBodies; i++)
    {
        delete[] pFrame->RigidBodies[i].Markers;
        delete[] pFrame->RigidBodies[i].MarkerIDs;
        delete[] pFrame->RigidBodies[i].MarkerSizes;
    }

}

bool init()
{
    printf("Graphic Vendor  : %s\n", glGetString(GL_VENDOR));
    printf("Graphic Renderer: %s\n", glGetString(GL_RENDERER));
    printf("GL  : %s\n", glGetString(GL_VERSION));
    printf("GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    printf("GLFW: %s\n", glfwGetVersionString());
    printf("GLEW: %s\n", glewGetString(GLEW_VERSION));
    
    unsigned char ver[4];
    server->NatNetVersion(ver);
    printf("NatNet: %d.%d.%d.%d\n", ver[0], ver[1], ver[2], ver[3]);

    // Init OpenGL state
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glClearColor(0, 0, 0, 1);

    if (!sm.loadShader("color-flat"))
        return false;

    plane.create(1, 1);

    proj = glm::ortho(0, 1, 1, 0, -1, 1);

    return true;
}

void render()
{
    glViewport(0, 0, g_width, g_height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glm::vec3 pos(Cursor::pos / glm::vec2(g_width, g_height), 0);
    plane.model = glm::translate(pos) * glm::scale(glm::vec3(.1f));
    sm.useShader("color-flat");
    sm.uniformMatrix4f("proj", proj);
    sm.uniformMatrix4f("modelview", plane.model);
    sm.uniform4f("color", glm::vec4(1));
    plane.draw(GL_TRIANGLES);
}

void window_resize(GLFWwindow* window, int w, int h)
{
    g_width = w;
    g_height = h;
}

void window_key(GLFWwindow* window,
    int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
    {
        switch (key)
        {
        case GLFW_KEY_ESCAPE:
            active = false;
            break;
         default:
            break;
        }
    }
}

void window_cursor(GLFWwindow* window, double x, double y)
{
    glm::vec2 oldPos(Cursor::pos);
    Cursor::pos = glm::vec2(x, y);
    mouseLeft.dragDelta = mouseLeft.down ? Cursor::pos - mouseLeft.dragStart : glm::vec2();
    mouseRight.dragDelta = mouseRight.down ? Cursor::pos - mouseRight.dragStart : glm::vec2();
    //if (mouseLeft.down)
    //    camera_rot += glm::radians(Cursor::pos - oldPos);
}

void window_button(GLFWwindow* window, int button, int action, int mods)
{
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT:
        mouseLeft.down = (action == GLFW_PRESS);
        mouseLeft.dragStart = Cursor::pos;
        break;
    case GLFW_MOUSE_BUTTON_RIGHT:
        mouseRight.down = (action == GLFW_PRESS);
        mouseRight.dragStart = Cursor::pos;
        break;
    }
}

void window_close(GLFWwindow* window)
{
    active = false;
}

int main(int argc, char* argv[])
{
    if (!glfwInit())
    {
        printf("GLFW init error\n");
        return -1;
    }

    // Destroy any existing window
    if (window)
        glfwDestroyWindow(window);
    // Create the window, fullscreen mode if shell opened
    if (!(window = glfwCreateWindow(g_width, g_height, "NatNetSimulator", nullptr, nullptr)))
    {
        printf("Window creation error\n");
        return -1;
    }

    glfwSetWindowCloseCallback(window, window_close);
    glfwSetWindowSizeCallback(window, window_resize);
    glfwSetKeyCallback(window, window_key);
    glfwSetCursorPosCallback(window, window_cursor);
    glfwSetMouseButtonCallback(window, window_button);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(0);

    // Init OpenGL extensions
    glewInit();
    //wglSwapIntervalEXT(-1);

    server = std::make_unique<NatNetServer>(ConnectionType::ConnectionType_Multicast);
    server->SetErrorMessageCallback(ServerLog);
    server->SetVerbosityLevel(Verbosity_Debug);
    server->SetMessageResponseCallback(ServerRequestHandler);
    int retCode = server->Initialize("127.0.0.1");
    if (server->Initialize("127.0.0.1") != 0)
    {
        printf("Error initializing server.  See log for details.  Exiting");
        return -1;
    }
    BuildDescription(&descriptions);

    // Init the scene
    if (!init())
    {
        printf("Init scene error\n");
        return -1;
    }

    // Main loop
    active = true;
    double start = glfwGetTime();
    while (!glfwWindowShouldClose(window) && active)
    {

        render();
        glfwPollEvents();
        glfwSwapBuffers(window);

        double diff = glfwGetTime() - start;
        if (diff > 0.1f)
        {
            sFrameOfMocapData frame;
            BuildFrame(g_lCurrentFrame, &descriptions, &frame);
            SendFrame(&frame);
            FreeFrame(&frame);
            g_lCurrentFrame++;
            printf("tick\n");
            start += 0.03f;
        }
    }
    
    active = false;

    glfwMakeContextCurrent(nullptr);
    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}

